
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#define WIFI_SSID "Madhouse"
#define WIFI_PASS "Password1234"
#define WIFI_CONN_BACKOFF_MS 1000
#define MQTT_SERVER_IP "192.168.1.188"
#define MQTT_SERVER_PORT 1883
const int STATUS_LED_PIN = 16;
const int TEMP_PIN = 4;
const int LIGHT_PIN = 17;
const float MIN_LIGHT_VALUE = 800.0;
const float MAX_LIGHT_VALUE = 1024.0;
float tempCelcius = 0.0f;
float tempFaren = 0.0f;
String deviceId;
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);
OneWire oneWire(TEMP_PIN);
DallasTemperature sensors(&oneWire);

// Forward decls
void connectWifi(const char* ssid, const char* password);
void printValuesToSerial(float tempCelcius, float tempFaren, float lightValue);
void sendMetrics(float tempCelcius, float tempFahrenheit, float lightValue);
String getDeviceId();
void setup() {
    Serial.begin(115200);
    // Turn off WiFi to resolve an issue with unstable readings
    // See this: https://github.com/esp8266/Arduino/issues/2070
    WiFi.disconnect(true);
    mqttClient.setServer(MQTT_SERVER_IP, MQTT_SERVER_PORT);
    pinMode(STATUS_LED_PIN, OUTPUT);
    deviceId = getDeviceId();
    sensors.begin();
    connectWifi(WIFI_SSID, WIFI_PASS);
}
void loop() {
    if ((WiFi.status() != WL_CONNECTED))
    {
        Serial.print("not connected, status == ");
        Serial.println(WiFi.status());
        delay(1000);
        return;
        //connectWifi(WIFI_SSID, WIFI_PASS);
    }
    sensors.requestTemperatures();
    float temperatureC = sensors.getTempCByIndex(0);
    float temperatureF = sensors.getTempFByIndex(0);
    // read the A0 pin for the light sensor
    float lightValue = analogRead(LIGHT_PIN);
    //clamp the analog reading to 0-100
    float lightLevel = 100 - ((lightValue - MIN_LIGHT_VALUE) * 100 / (MAX_LIGHT_VALUE - MIN_LIGHT_VALUE));
    printValuesToSerial(temperatureC, temperatureF, lightLevel);
    sendMetrics(temperatureC, temperatureF, lightLevel);
    delay(5000);
}
void connectWifi(const char* ssid, const char* password) {
    WiFi.mode(WIFI_STA);
    WiFi.setAutoReconnect(true);
    WiFi.begin(ssid, password);
    Serial.print("Connecting WiFi");
    while (WiFi.isConnected() != true) {
        Serial.print(".");
        delay(WIFI_CONN_BACKOFF_MS);
    }
    Serial.print("IP=");
    Serial.println(WiFi.localIP());
}
void printValuesToSerial(float temperatureC, float temperatureF, float lightValue) {
    Serial.print(temperatureC);
    Serial.println("ºC");
    Serial.print(temperatureF);
    Serial.println("ºF");
    Serial.println(lightValue);
}
void sendMetrics(float temperatureC, float temperatureF, float lightValue) {
    digitalWrite(STATUS_LED_PIN, LOW);
    if (mqttClient.connect(deviceId.c_str(), "", "")) {
        mqttClient.publish((deviceId + "/temp_fahrenheit").c_str(), String(temperatureF).c_str(), true);
        mqttClient.publish((deviceId + "/temp_celcius").c_str(), String(temperatureC).c_str(), true);
        mqttClient.publish((deviceId + "/light_sensor").c_str(), String(lightValue).c_str(), true);
    } else {
        Serial.print("MQTT Send failed\n");
    }
    digitalWrite(STATUS_LED_PIN, HIGH);
}
String getDeviceId() {
    return ("iot_" + WiFi.macAddress());
}

